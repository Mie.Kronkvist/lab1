package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/1/
 */


 //heihei
public class Lab1 {
    
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        task1();
        task2();
        
    }

    public static void task1() {
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line
        
         // Les inn brukerens navn ved å bruke readInput
         String navn = readInput("Hva er ditt navn?");
         // Les inn brukerens gatedresse ved å bruke readInput
         String adresse = readInput("Hva er din adresse?");
         // Les inn postkode og poststed ved å bruke readInput
         String postInfo = readInput("Hva er ditt postnummer og poststed?");
 
         // Printer "navn-s adresse er:"
         System.out.println(navn + "-s adresse er:");
         // Printer en tom linje
         System.out.println();
         // Printer navn og adresse slik man skriver det på et brev
         System.out.println(navn);
         System.out.println(adresse);
         System.out.println(postInfo);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

}


