package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        testRemoveRow();
        testAllRowsAndColsAreEqualSum();
    }

    private static void testRemoveRow() {
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid1, 0);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }
        // [21, 22, 23]
        // [31, 32, 33]

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid2.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid2.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid2, 1);
        for (int i = 0; i < grid2.size(); i++) {
            System.out.println(grid2.get(i));
        }
        // [11, 12, 13]
        // [31, 32, 33]
    }

    private static void testAllRowsAndColsAreEqualSum() {
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        boolean equalSums1 = allRowsAndColsAreEqualSum(grid1);
        System.out.println(equalSums1); // false

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grid2.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grid2.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

        boolean equalSums2 = allRowsAndColsAreEqualSum(grid2);
        System.out.println(equalSums2); // false

        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grid3);
        System.out.println(equalSums3); // true
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (row < 0 || row >= grid.size()) {
            throw new IllegalArgumentException("Invalid row index");
        }

        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (grid.isEmpty() || grid.get(0).isEmpty()) {
            throw new IllegalArgumentException("Grid cannot be empty");
        }

        int expectedRowSum = rowSum(grid.get(0));

        // Check row sums
        for (ArrayList<Integer> row : grid) {
            if (rowSum(row) != expectedRowSum) {
                return false;
            }
        }

        // Check column sums
        for (int col = 0; col < grid.get(0).size(); col++) {
            int sum = 0;
            for (ArrayList<Integer> row : grid) {
                sum += row.get(col);
            }
            if (sum != expectedRowSum) {
                return false;
            }
        }

        return true;
    }

    private static int rowSum(ArrayList<Integer> row) {
        int sum = 0;
        for (int value : row) {
            sum += value;
        }
        return sum;
    }
}



