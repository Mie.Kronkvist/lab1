package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    

    
        public static void main(String[] args) {
            // Test removeThrees
            testRemoveThrees();
    
            // Test uniqueValues
            testUniqueValues();
    
            // Test addList
            testAddList();
        }
    
        private static void testRemoveThrees() {
            List<Integer> list1 = Arrays.asList(1, 2, 3, 4);
            ArrayList<Integer> arrayList1 = new ArrayList<>(list1);
            ArrayList<Integer> removedList1 = removeThrees(arrayList1);
            System.out.println(removedList1); // [1, 2, 4]
    
            List<Integer> list2 = Arrays.asList(1, 2, 3, 3);
            ArrayList<Integer> arrayList2 = new ArrayList<>(list2);
            ArrayList<Integer> removedList2 = removeThrees(arrayList2);
            System.out.println(removedList2); // [1, 2]
    
            List<Integer> list3 = Arrays.asList(3, 3, 1, 3, 2, 4, 3, 3, 3);
            ArrayList<Integer> arrayList3 = new ArrayList<>(list3);
            ArrayList<Integer> removedList3 = removeThrees(arrayList3);
            System.out.println(removedList3); // [1, 2, 4]
    
            List<Integer> list4 = Arrays.asList(3, 3);
            ArrayList<Integer> arrayList4 = new ArrayList<>(list4);
            ArrayList<Integer> removedList4 = removeThrees(arrayList4);
            System.out.println(removedList4); // []
        }
    


        private static void testUniqueValues() {
            List<Integer> list5 = Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2);
            ArrayList<Integer> arrayList5 = new ArrayList<>(list5);
            List<Integer> uniqueList1 = uniqueValues(arrayList5);
            System.out.println(uniqueList1); // [1, 2, 3]
        
            List<Integer> list6 = Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 5);
            ArrayList<Integer> arrayList6 = new ArrayList<>(list6);
            List<Integer> uniqueList2 = uniqueValues(arrayList6);
            System.out.println(uniqueList2); // [4, 5]
        }



        private static void testAddList() {
            // Test case 1
            ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(1, 2, 3));
            ArrayList<Integer> b1 = new ArrayList<>(Arrays.asList(4, 2, -3));
            addList(a1, b1);
            System.out.println(a1); // [5, 4, 0]
        
            // Test case 2
            ArrayList<Integer> a2 = new ArrayList<>(Arrays.asList(1, 2, 3));
            ArrayList<Integer> b2 = new ArrayList<>(Arrays.asList(47, 21, -30));
            addList(a2, b2);
            System.out.println(a2); // [48, 23, -27]
        }



    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> result = new ArrayList<>();

        for (Integer num : list) {
            if (num != 3) {
                result.add(num);
            }
        }

        return result;
    }


    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> result = new ArrayList<>();

        for (Integer num : list) {
            if (!result.contains(num)) {
                result.add(num);
            }
        }

        return result;
    }



    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b){
        // Sjekker at begge listene har samme lengde
        if (a.size() != b.size()) {
            throw new IllegalArgumentException("Listene må ha samme lengde");
        }

        // Itererer gjennom listene og legger sammen verdiene
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }
}