package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */

 //oppgave 1

public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Game", "Action", "Champion");
        findLongestWords("apple", "carrot", "ananas");
        findLongestWords("Four", "Five", "Nine");






//oppgave 2

     boolean leapYear1 = isLeapYear(2022);
    System.out.println(leapYear1); // false

    boolean leapYear2 = isLeapYear(1996);
    System.out.println(leapYear2); // true

    boolean leapYear3 = isLeapYear(1900);
    System.out.println(leapYear3); // false

    boolean leapYear4 = isLeapYear(2000);
    System.out.println(leapYear4); // true
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int maxLength = 0;

        // Finn maksimal lengde blant ordene
        if (word1.length() > maxLength) {
            maxLength = word1.length();
        }
        if (word2.length() > maxLength) {
            maxLength = word2.length();
        }
        if (word3.length() > maxLength) {
            maxLength = word3.length();
        }
    
        // Skriv ut ordene med maksimal lengde
        if (word1.length() == maxLength) {
            System.out.println(word1);
        }
        if (word2.length() == maxLength) {
            System.out.println(word2);
        }
        if (word3.length() == maxLength) {
            System.out.println(word3);
        }
    }
    

    public static boolean isLeapYear(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            return true;
        } else {
            return false;
        }
    }




    public static boolean isEvenPositiveInt(int num){
        return num > 0 && num % 2 == 0;
    }
}
